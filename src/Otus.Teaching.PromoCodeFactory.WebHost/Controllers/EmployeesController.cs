﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создание нового сотрудника
        /// </summary>
        /// <param name="firstName">Имя</param>
        /// <param name="lastName">Фамилия</param>
        /// <param name="email">E-mail</param>
        /// <param name="roleId">Id роли</param>
        /// <param name="appliedPromocodesCount">Количество промокодов</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<string> CreateEmployeeAsync(string firstName, string lastName, string email, Guid roleId, int appliedPromocodesCount)
        {
            List<Role> Roles = FakeDataFactory.Roles.Where(x => x.Id == roleId).ToList();
            
            
            Employee newEmployee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Roles = Roles,
                AppliedPromocodesCount = appliedPromocodesCount
            };

            try
            {
                if (await _employeeRepository.CreateAsync(newEmployee))
                {
                    return "Сотрудник успешно создан";
                }

                return "Ошибка создания сотрудника";
            }
            catch (Exception e)
            {
                return "Ошибка создания сотрудника";
            }
        }


        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<string> DeleteEmployeeByIdAsync(Guid id)
        {
            try
            {
                if (await _employeeRepository.DeleteByIdAsync(id))
                {
                    return "Сотрудник успешно удален";
                }

                return "Ошибка при удалении сотрудника";
            }
            catch (Exception e)
            {
                return "Ошибка при удалении сотрудника";
            }

        }


        /// <summary>
        /// Изменение сотрудника
        /// </summary>
        /// <param name="id">Id сторудника</param>
        /// <param name="firstName">Имя</param>
        /// <param name="lastName">Фамилия</param>
        /// <param name="email">E-mail</param>
        /// <param name="roleId">Id роли</param>
        /// <param name="appliedPromocodesCount">Количество промокодов</param>
        /// <returns></returns>
        [HttpPatch]
        public async Task<string> UpdateEmployeeAsync(Guid id, string firstName, string lastName, string email, Guid roleId, int appliedPromocodesCount)
        {
            List<Role> Roles = FakeDataFactory.Roles.Where(x => x.Id == roleId).ToList();

            Employee updateEmployee = new Employee()
            {
                Id = id,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Roles = Roles,
                AppliedPromocodesCount = appliedPromocodesCount
            };

            try
            {
                if (await _employeeRepository.UpdateByIdAsync(updateEmployee))
                {
                    return "Сотрудник успешно изменен";
                }

                return "Ошибка изменения сотрудника";
            }
            catch (Exception e)
            {
                return "Ошибка изменения сотрудника";
            }
        }

    }
}